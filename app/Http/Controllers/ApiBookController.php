<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;

class ApiBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $books = Book::paginate(10);
         $books = Book::all();
        if (!$books) {
            throw new HttpException(400, "Invalid data");
        }
        return response()->json(
            $books,
            200
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $book = new Book;
        $book->name = $request->input('name');
        $book->price = $request->input('price');
        $book->content = $request->input('content');
        if ($book->save()) {
            return $book;
        }
        throw new HttpException(400, "Invalid data");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        if (!$id) {
           throw new HttpException(400, "Invalid id");
        }
        $book = Book::find($id);
        return response()->json([
            $book,
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
           if (!$id) {
            throw new HttpException(400, "Invalid id");
        }
        $book = Book::find($id);
        $book->name = $request->input('name');
        $book->price = $request->input('price');
        $book->content = $request->input('content');
        if ($book->save()) {
            return $book;
        }
        throw new HttpException(400, "Invalid data");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         if (!$id) {
            throw new HttpException(400, "Invalid id");
        }
        $book = Book::find($id);
        $book->delete();
        return response()->json([
            'message' => 'book deleted',
        ], 200);
    }
}
