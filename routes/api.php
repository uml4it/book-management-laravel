<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//Route::group(['middleware' => 'auth.basic'], function () {
    Route::get('/books', 'ApiBookController@index');
    Route::get('/books/{id}', 'ApiBookController@show');
    Route::post('/books', 'ApiBookController@store');
    Route::put('/books/{id}', 'ApiBookController@update');
    Route::delete('/books/{id}', 'ApiBookController@destroy');
//});